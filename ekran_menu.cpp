#include "ekran_menu.h"
#include "wspolne.h"

extern bool gra_trwa;

bool menuKlawisz(char klawisz)
{
	int wybor = -1;
	if(klawisz >= '1' && klawisz <= '3')
		wybor = klawisz - '0';
	else
		return false;

	switch(wybor)
	{
	case GRAJ:
		zmienEkran(GRAJ);
		break;
	case AUTORZY:
		zmienEkran(AUTORZY);
		break;
	case WYJDZ: {
		system("cls");
		wcout << L"Dziękujemy za wspólną grę!" << endl;
		gra_trwa = false;
		break;
	}
	}

	return true;
}

void menuEkran()
{
	wcout << L"1. Zagraj" << endl;
	wcout << L"2. Autorzy" << endl;
	wcout << L"3. Wyjdź" << endl;

	wcout << endl << L"Wybierz opcję z menu: ";
}
