#ifndef WSPOLNE_H
#define WSPOLNE_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <stdlib.h>
#include <conio.h>
#include <ctime>
#include <windows.h>
#include "kolory.h"

using namespace std;

#define MENU 0
#define GRAJ 1
#define AUTORZY 2
#define WYJDZ 3

void zmienEkran(unsigned nowy_ekran);

#endif // WSPOLNE_H
