#ifndef KOLORY_H
#define KOLORY_H

#include <string>

// wzięte z neta
// poszczególne numerki dla kolorów w konsoli windowsa
enum Kolory
{
	black=0,
	dark_blue=1,
	dark_green=2,
	dark_aqua,dark_cyan=3,
	dark_red=4,
	dark_purple=5,dark_pink=5,dark_magenta=5,
	dark_yellow=6,
	dark_white=7,
	gray=8,
	blue=9,
	green=10,
	aqua=11,cyan=11,
	red=12,
	purple=13,pink=13,magenta=13,
	yellow=14,
	white=15
};

void wypiszKolorowyTekst(std::wstring tekst, Kolory color);
void wypiszKolorowyTekst(std::string tekst, Kolory color);

#endif // KOLORY_H
