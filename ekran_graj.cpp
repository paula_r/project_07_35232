#include "ekran_graj.h"
#include "wspolne.h"
#include <fstream>
#include <vector>
#include <sstream>

enum Widok
{
	ZaznaczanieOdpowiedzi,
	PotwierdzanieOdpowiedzi,
	OdpowiedzPoprawna,
	OdpowiedzBledna,
	Rezygnacja,
	KoloRatunkowe_Telefon,
	KoloRatunkowe_50na50,
	KoloRatunkowe_Publicznosc,
	WygranaMiliona
};

struct Gra
{
	Widok widok; // [ZaznaczanieOdpowiedzi...WygranaMiliona]
	unsigned numer_pytania; // [1...15]
	unsigned wygrana; // [100...1000000]
	unsigned zaznaczona_odpowiedz; // [1...4]
	bool koloTelefon; // czy telefon dostepny
	unsigned podpowiedz; // odpowiedź od przyjaciela [1...4]
	bool kolo50_50; // czy 50 na 50 dostepne
	bool koloPublicznosc; // czy publiczność dostepna
	unsigned podpowiedzPublicznosci[4]; // procenty odpowiedzi od publiczności [0...100]
} gra;


#define ILOSC_PYTAN 15
const unsigned sumy_pieniezne[ILOSC_PYTAN]
{
	100,
	200,
	300,
	500,
	1000,
	2000,
	4000,
	8000,
	16000,
	32000,
	64000,
	125000,
	250000,
	500000,
	1000000
};
unsigned sumaPienieznaZaObecnePytanie()
{
	return sumy_pieniezne[gra.numer_pytania - 1];
}

#define SUMY_GWARANTOWANE 2
const unsigned sumy_gwarantowane[SUMY_GWARANTOWANE]
{
	1000,
	32000
};


struct Pytanie
{
	string pytanie;
	string odpowiedzi[4];
	bool wyswietlaj_odpowiedz[4]; // wyswietlaj_odpowiedz[0...3] -> zawiera false gdy odpowiedź odrzucona przy "50na50" i ma się nie wyświetlać
	unsigned prawidlowa_odpowiedz;

	bool czyPoprawnaOdpowiedz() { return gra.zaznaczona_odpowiedz == prawidlowa_odpowiedz; }
} pytanie;

enum
{
	ID,
	QUESTION,
	ANSWER1,
	ANSWER2,
	ANSWER3,
	ANSWER4,
	CORRECT_ANSWER
};


void wczytajPytanie()
{
	string nazwa_pliku = "csv/" + to_string(gra.numer_pytania) + ".csv";
	ifstream plik(nazwa_pliku);
	if(!plik.is_open()) {
		system("cls");
		wcout << L"Nie można otworzyć pliku: ";
		cout << nazwa_pliku << endl;
		wcout << L"Gra przerwana." << endl;
		exit(EXIT_FAILURE);
	}

	vector<vector<string>> pytania;
	vector<string> wiersz;
	string linia, kolumna;
	getline(plik, linia); // pominięcie pierwszej linii z nazwami kolumn danych
	while(getline(plik, linia))
	{
		wiersz.clear();
		stringstream stream(linia);

		while(getline(stream, kolumna, ';'))
			wiersz.push_back(kolumna);
		pytania.push_back(wiersz);
	}

	unsigned wylosowane_pytanie = rand() % pytania.size();
	pytanie.pytanie = pytania[wylosowane_pytanie][QUESTION];
	for(int i = 0; i < 4; i++) {
		pytanie.odpowiedzi[i] = pytania[wylosowane_pytanie][ANSWER1 + i];
		pytanie.wyswietlaj_odpowiedz[i] = true;
	}
	pytanie.prawidlowa_odpowiedz = stoul(pytania[wylosowane_pytanie][CORRECT_ANSWER]); // stoul(...) rzutuje string'a na unsigned longa
	// rzutujemy na unsigned longa żeby potem zapisać wynik do 'prawidlowa_odpowiedz', które jest typu unsigned (ten sam typ znakowy)

	gra.zaznaczona_odpowiedz = 0;
	gra.widok = ZaznaczanieOdpowiedzi;
}

void rozpocznijGre()
{
	gra.numer_pytania = 1;
	gra.wygrana = 0;
	gra.koloTelefon = true;
	gra.kolo50_50 = true;
	gra.koloPublicznosc = true;

	wczytajPytanie();
}

bool grajKlawisz(char klawisz)
{
	int wybor = -1;

	switch(gra.widok)
	{
	case ZaznaczanieOdpowiedzi:
	case KoloRatunkowe_Telefon:
	case KoloRatunkowe_Publicznosc:
	case KoloRatunkowe_50na50: {
		if(gra.numer_pytania > 1 && (klawisz == 'r' || klawisz == 'R')) {
			gra.widok = Rezygnacja;
			return true;
		}

		if(klawisz == '1') {
			if(!gra.koloTelefon) {
				wcout << klawisz << endl;
				wypiszKolorowyTekst(L"Telefon do przyjaciela już został wykorzystany.", Kolory::red);
				return false;
			}

			if(gra.numer_pytania <= 5) {
				gra.podpowiedz = pytanie.prawidlowa_odpowiedz;
			} else {
				unsigned wylosowane;
				do {
					wylosowane = rand() % 4;
				} while(!pytanie.wyswietlaj_odpowiedz[wylosowane]);
				gra.podpowiedz = wylosowane + 1;
			}

			gra.koloTelefon = false;
			gra.widok = KoloRatunkowe_Telefon;
			return true;
		} else if(klawisz == '2') {
			if(!gra.kolo50_50) {
				wcout << klawisz << endl;
				wypiszKolorowyTekst(L"50 na 50 już zostało wykorzystane.", Kolory::red);
				return false;
			}

			unsigned usuniete = 0;
			while(usuniete < 2) {
				unsigned wylosowane = rand() % 4;
				if(wylosowane + 1 != pytanie.prawidlowa_odpowiedz && pytanie.wyswietlaj_odpowiedz[wylosowane]) {
					pytanie.wyswietlaj_odpowiedz[wylosowane] = false;
					usuniete++;
				}
			}

			gra.kolo50_50 = false;
			gra.widok = KoloRatunkowe_50na50;
			return true;
		} else if(klawisz == '3') {
			if(!gra.koloPublicznosc) {
				wcout << klawisz << endl;
				wypiszKolorowyTekst(L"Pytanie do publiczności już zostało wykorzystane.", Kolory::red);
				return false;
			}

			unsigned suma_procent = rand() % 51 + 50; // wylosowanie procentów dla poprawnej odpowiedzi
			gra.podpowiedzPublicznosci[pytanie.prawidlowa_odpowiedz - 1] = suma_procent;

			// sprawdzenie czy w tym pytaniu została wykorzystane 50na50
			unsigned wyswietlane = 0, n_wyswietlane = 0;
			for(unsigned i = 0; i < 4; i++) {
				if(pytanie.wyswietlaj_odpowiedz[i]) {
					wyswietlane++;
					if(i + 1 != pytanie.prawidlowa_odpowiedz)
						n_wyswietlane = i;
				}
			}

			if(wyswietlane == 2) {
				gra.podpowiedzPublicznosci[n_wyswietlane] = 100 - suma_procent;
			} else {
				for(unsigned i = 0; i < 4; i++) {
					if(i + 1 == pytanie.prawidlowa_odpowiedz) // dla prawidłowej procenty już wylosowane
						continue;
					else if(i == 3) // dla ostatniej odpowiedzi pozostałe punkty procentowe
						gra.podpowiedzPublicznosci[i] = 100 - suma_procent;
					else { // losujemy procenty
						unsigned procenty = 0;
						if(100 - suma_procent + 1 > 0) // zabezpieczenie żeby nie dzielić przez 0
							procenty = rand() % (100 - suma_procent + 1);
						gra.podpowiedzPublicznosci[i] = procenty;
						suma_procent += procenty;
					}
				}
			}

			gra.koloPublicznosc = false;
			gra.widok = KoloRatunkowe_Publicznosc;
			return true;
		}

		if(klawisz >= 'a' && klawisz <= 'd')
			wybor = klawisz - 'a';
		else if(klawisz >= 'A' && klawisz <= 'D')
			wybor = klawisz - 'A';
		else {
			cout << klawisz;
			return false;
		}

		if(!pytanie.wyswietlaj_odpowiedz[wybor]) {
			cout << klawisz << endl;
			wypiszKolorowyTekst(L"Ta odpowiedź została już odrzucona.", Kolory::red);
			return false;
		}

		gra.zaznaczona_odpowiedz = ++wybor;
		gra.widok = PotwierdzanieOdpowiedzi;
		break;
	}
	case PotwierdzanieOdpowiedzi: {
		if(gra.numer_pytania > 1 && (klawisz == 'r' || klawisz == 'R')) {
			gra.widok = Rezygnacja;
			return true;
		}
		if(klawisz == 'b' || klawisz == 'B') { // zmiana zaznaczonej odpowiedzi
			gra.widok = ZaznaczanieOdpowiedzi;
			return true;
		}

		if(klawisz != '\r') // nie wciśnięty enter
			return false;

		if(pytanie.czyPoprawnaOdpowiedz()) {
			for(int i = 0; i < SUMY_GWARANTOWANE; i++) {
				if(sumaPienieznaZaObecnePytanie() == sumy_gwarantowane[i]) {
					gra.wygrana = sumaPienieznaZaObecnePytanie();
					break;
				}
			}
			if(gra.numer_pytania + 1 > ILOSC_PYTAN) {
				gra.widok = WygranaMiliona;
			} else {
				gra.widok = OdpowiedzPoprawna;
			}
		} else {
			gra.widok = OdpowiedzBledna;
		}
		break;
	}
	case OdpowiedzPoprawna: {
		gra.numer_pytania++;
		wczytajPytanie();
		break;
	}
	case OdpowiedzBledna:
	case Rezygnacja:
	case WygranaMiliona: {
		zmienEkran(MENU);
		break;
	}
	}

	return true;
}

void kolaRatunkowe()
{
	wcout << L"Koła ratunkowe:" << endl;
	wcout << L"1: Telefon do przyjaciela: ";
	wypiszKolorowyTekst((gra.koloTelefon ? L"dostępne" : L"niedostępne"), (gra.koloTelefon ? Kolory::green : Kolory::red));
	wcout << endl << L"2: 50 na 50: ";
	wypiszKolorowyTekst((gra.kolo50_50 ? L"dostępne" : L"niedostępne"), (gra.kolo50_50 ? Kolory::green : Kolory::red));
	wcout << endl << L"3: Pytanie do publiczności: ";
	wypiszKolorowyTekst((gra.koloPublicznosc ? L"dostępne" : L"niedostępne"), (gra.koloPublicznosc ? Kolory::green : Kolory::red));
	cout << endl << endl << endl;
}

void pytanieOrazOdpowiedzi()
{
	wcout << "Pytanie numer " << gra.numer_pytania << ". " << "za ";
	wypiszKolorowyTekst(to_wstring(sumaPienieznaZaObecnePytanie()) + L" zł", Kolory::yellow);
	cout << endl << endl;
	cout << pytanie.pytanie << endl;

	for(unsigned i = 0; i < 4; i++) {
		if(pytanie.wyswietlaj_odpowiedz[i]) {
			Kolory kolor;
			if((gra.widok == OdpowiedzPoprawna || gra.widok == OdpowiedzBledna)  && i + 1 == pytanie.prawidlowa_odpowiedz)
				kolor = Kolory::green;
			else if(gra.widok >= PotwierdzanieOdpowiedzi && gra.widok <= OdpowiedzBledna  && i + 1 == gra.zaznaczona_odpowiedz)
				kolor = Kolory::dark_yellow;
			else
				kolor = Kolory::white;
			string str;
			str += (char)('A' + i);
			str += ": " + pytanie.odpowiedzi[i];
			wypiszKolorowyTekst(str, kolor);
			cout << endl;
		} else
			cout << endl;
	}
	cout << endl << endl;
}

void grajEkran()
{
	switch(gra.widok)
	{
	case ZaznaczanieOdpowiedzi: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		if(gra.numer_pytania > 1)
			wcout << L"Jeśli chcesz zrezygnować z gry, wciśnij klawisz 'R'." << endl;
		wcout << L"Wybierz odpowiedź: ";
		break;
	}
	case PotwierdzanieOdpowiedzi: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		const char wybrana_odpowiedz = (char)('A' + gra.zaznaczona_odpowiedz - 1);
		wcout << L"Czy wybierasz odpowiedź '" << wybrana_odpowiedz << L"' definitywnie? Wciśnij ENTER." << endl;
		wcout << L"Jeśli chcesz zmienić odpowiedź, wciśnij 'B'.";
		if(gra.numer_pytania > 1)
			wcout << endl << L"Jeśli chcesz zrezygnować z gry, wciśnij klawisz 'R'." << endl;
		break;
	}
	case OdpowiedzPoprawna: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		wypiszKolorowyTekst(L"To poprawna odpowiedź! Masz " + to_wstring(sumaPienieznaZaObecnePytanie()) + L" zł", Kolory::green);
		wcout << endl << L"Naciśnij dowolny klawisz, aby kontynuować grę.";
		break;
	}
	case OdpowiedzBledna: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		wypiszKolorowyTekst(L"Niestety to błędna odpowiedź :(", Kolory::red);
		wcout << endl << L"Twoja wygrana w Milionerach to: " << gra.wygrana << L" zł";
		break;
	}
	case Rezygnacja: {
		wcout << L"Twoja wygrana w Milionerach to: " << sumy_pieniezne[gra.numer_pytania - 2] << L" zł";
		break;
	}
	case KoloRatunkowe_Telefon: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		wcout << L"Twój przyjaciel wybrał odpowiedź: " << (char)('A' + gra.podpowiedz - 1) << endl;
		wcout << L"Wybierz odpowiedź: ";
		break;
	}
	case KoloRatunkowe_50na50: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		wcout << L"Odrzucono dwie nieprawidłowe odpowiedzi." << endl;
		wcout << L"Wybierz odpowiedź: ";
		break;
	}
	case KoloRatunkowe_Publicznosc: {
		kolaRatunkowe();
		pytanieOrazOdpowiedzi();
		wcout << L"Publiczność wskazała następujące odpowiedzi:" << endl;
		for(unsigned i = 0; i < 4; i++) {
			if(!pytanie.wyswietlaj_odpowiedz[i])
				continue;
			cout << (char)('A' + i) << ": " << gra.podpowiedzPublicznosci[i] << "%" << endl;
		}
		wcout << endl << L"Wybierz odpowiedź: ";
		break;
	}
	case WygranaMiliona: {
		wcout << L"WYGRAŁEŚ MILION ZŁOTYCH!!!" << endl;
		wcout << L"Gratulacje!";
		break;
	}
	}
}
