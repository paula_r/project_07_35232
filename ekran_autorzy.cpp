#include "ekran_menu.h"
#include "wspolne.h"

bool autorzyKlawisz(char klawisz)
{
	zmienEkran(MENU);
	return true;
}

void autorzyEkran()
{
	wcout << L"Autorzy:" << endl;
	wcout << L"\tKacper Żurowski" << endl;
	wcout << L"\tPaulina Radzik" << endl;
	wcout << L"\tWiktor Uriasz" << endl;

	wcout << endl << endl << L"Wciśnij dowolny klawisz, aby powrócić do menu.";
}
