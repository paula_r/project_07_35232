#include "wspolne.h"
#include "ekran_menu.h"
#include "ekran_graj.h"
#include "ekran_autorzy.h"

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
bool gra_trwa = true;
unsigned ekran = 0;

void wypiszKolorowyTekst(wstring tekst, Kolory color)
{
	SetConsoleTextAttribute(hConsole, color);
	wcout << tekst;
	SetConsoleTextAttribute(hConsole, Kolory::white);
}

void wypiszKolorowyTekst(string tekst, Kolory color)
{
	SetConsoleTextAttribute(hConsole, color);
	cout << tekst;
	SetConsoleTextAttribute(hConsole, Kolory::white);
}

void zmienEkran(unsigned nowy_ekran)
{
	ekran = nowy_ekran;
	switch(ekran)
	{
	case MENU: {
		break;
	}
	case GRAJ: {
		rozpocznijGre();
		break;
	}
	case AUTORZY: {
		break;
	}
	}
}

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, ""); // do wyświetlania polskiach znaków

	char klawisz;

	while(gra_trwa)
	{
		system("cls"); // czyszczenie konsoli

		// wypisywanie zawartości ekranu
		switch(ekran)
		{
		case MENU: { // wyświetl menu
			menuEkran();
			break;
		}
		case GRAJ: { // wyświetl grę
			grajEkran();
			break;
		}
		case AUTORZY: { // wyświetl creditsy
			autorzyEkran();
			break;
		}
		}

		// obsługa klawiszy w danym ekranie
		bool dobry_klawisz = false;
		do {
			klawisz = getch(); // getch(), aby nie czekać na wciśnięcie Entera
			// jeśli chcemy z Enterem to: cin >> klawisz;

			switch(ekran)
			{
			case MENU: {
				dobry_klawisz = menuKlawisz(klawisz);
				break;
			}
			case GRAJ: {
				dobry_klawisz = grajKlawisz(klawisz);
				break;
			}
			case AUTORZY: {
				dobry_klawisz = autorzyKlawisz(klawisz);
				break;
			}
			}

			if(!dobry_klawisz)
				wcout << endl << L"Nieprawidłowa opcja! Wybierz poprawną opcję: ";
		} while(!dobry_klawisz);
	}

	return 0;
}
